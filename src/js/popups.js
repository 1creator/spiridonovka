function togglePopup(popup, value) {
    if (value === undefined) {
        value = !popup.classList.contains('show');
    }
    popup.classList.toggle('show', value);
}

let popups = document.getElementsByClassName('popup');
for (let popup of popups) {
    document.querySelectorAll(`[data-target=${popup.id}], [href=${popup.id}]`)
        .forEach(toggler => toggler.addEventListener('click', function () {
            togglePopup(document.getElementById(this.dataset.target));
        }));

    popup.querySelectorAll('[data-action="close"]').forEach(item => {
        item.addEventListener('click', function (event) {
            event.preventDefault();
            togglePopup(popup, false);
        });
    });
}