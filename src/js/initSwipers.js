import Swiper from 'swiper';

Swiper.instances = {};
let swipers = document.getElementsByClassName('swiper-container');
for (let swiper of swipers) {
    let preset = swiper.dataset.preset;
    let options;

    switch (preset) {
        case 'trainers':
            options = {
                slidesPerView: 1,
                spaceBetween: 15,
                navigation: {
                    prevEl: '.trainers-block__arrow_left',
                    nextEl: '.trainers-block__arrow_right',
                },
                pagination: {
                    el: '.trainers-block__pagination',
                    type: 'bullets',
                },
                observer: true,
                observeParents: true,
                breakpoints: {
                    760: {
                        slidesPerView: 3,
                    },
                    1024: {
                        slidesPerView: 4
                    },
                    1330: {
                        slidesPerView: 5
                    }
                }
            };
            break;
        default:
            let slidesPerView = parseInt(swiper.dataset.slidesperview || 1);
            let spaceBetween = parseInt(swiper.dataset.spacebetween || 0);
            let nextButton = swiper.dataset.next;
            let prevButton = swiper.dataset.prev;
            let paginationEl = swiper.dataset.paginationel;
            let paginationType = swiper.dataset.paginationtype;
            let direction = swiper.dataset.direction || 'horizontal';
            let name = swiper.dataset.name;
            let allowTouchMove = swiper.dataset.allowtouchmove !== 'false';
            let mousewheel = swiper.dataset.mousewheel ? {invert: false, releaseOnEdges: false} : false;

            options = {
                slidesPerView: slidesPerView,
                spaceBetween: spaceBetween,
                allowTouchMove: allowTouchMove,
                direction: direction,
                mousewheel: mousewheel,
                navigation: {
                    nextEl: nextButton,
                    prevEl: prevButton,
                },
                pagination: {
                    el: paginationEl,
                    type: paginationType,
                }
            };
            break;
    }

    let sw = new Swiper(swiper, options);

    if (name) {
        Swiper.instances[name] = sw;
    }
}

for (let swiper of swipers) {
    let name = swiper.dataset.name;
    let controlSwiper = swiper.dataset.controlswiper;

    if (controlSwiper) {
        Swiper.instances[name].controller.control = Swiper.instances[controlSwiper];
    }
}