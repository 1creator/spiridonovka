import './initSwipers';
import './modals';
import bsn from 'bootstrap.native/dist/bootstrap-native-v4';
import 'simplebar'

let trainersBlock = document.getElementById('trainersBlock');
let pricesBlock = document.getElementById('pricesBlock');
let expandBtn = document.getElementById('expandBtn');

function showTrainersBlock(smooth = true) {
    if (!smooth) {
        trainersBlock.style.transition = 'none';
    }
    trainersBlock.classList.toggle('show', true);
    trainersBlock.scrollIntoView({block: "start", behavior: "smooth"});
}

function showPricesBlock(smooth = true) {
    if (!smooth) {
        trainersBlock.style.transition = 'none';
        pricesBlock.style.transition = 'none';
    }
    trainersBlock.classList.toggle('show', true);
    pricesBlock.classList.toggle('show', true);
    expandBtn.remove();
    pricesBlock.scrollIntoView({block: "start", behavior: "smooth"});
}

expandBtn.addEventListener('click', function () {
    if (trainersBlock.classList.contains('show')) {
        showPricesBlock();
    } else {
        showTrainersBlock();
    }
});

window.onhashchange = function locationHashChanged() {
    if (location.hash === "#trainers") {
        showTrainersBlock(false);
    } else if (location.hash === "#prices") {
        showPricesBlock(false);
    }
};
